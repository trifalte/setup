Dir[File.dirname(__FILE__) + '/lib/*.rb'].each do |file| 
  require File.basename(file, File.extname(file))
end

Dir[File.dirname(__FILE__) + '/app/*.rb'].each do |file| 
  require File.basename(file, File.extname(file))
end