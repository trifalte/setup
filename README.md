# Setup
This is a simple setup script for a ruby project

It will:

* Create the project directories
* Install the appropriate gems
* Initialize git and push up the project to a repo on bitbucket

TODO:

* push to repos other than bitbucket
* initialize a project_name.rb file
* option for bundler using path/binstub for when not using .bundle/config