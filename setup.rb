#!/usr/bin/env ruby

require 'thor'

class Setup < Thor
  include Thor::Actions
  option :user, required: true, desc: "User for bitbucket", aliases: "-u"
  # option :source_host, required: false, desc: "Source code hosting service to use", aliases: "-sh"

  desc "new APP_NAME", "creates a new application"
  def new(project_name)
    Setup.source_root("#{File.expand_path(File.dirname(__FILE__))}")

    # First thing a project needs is its own folder
    say "creating project: #{project_name}"
    %W( #{project_name}/lib #{project_name}/app ).each do |dir|
      empty_directory(dir, recursive: true)
    end

    # Import templates
    say "adding templates"
    directory("templates/.", "#{project_name}")

    # TODO: create first .rb (and spec) based on project name

    inside "#{project_name}" do
      # Add bundler to the list of gems
      say "installing gems"
      `bundle install`

      # Git setup + push up private project to bitbucket
      # TODO: Support something besides bitbucket
      say "adding version control using git"
      `git init && git add . && git commit -m 'initial setup'`
      say "pushing up project to bitbucket"
       `curl --user '#{options[:user]}' https://api.bitbucket.org/1.0/repositories/ --data name='#{project_name}' --data is_private='true' --data language='ruby'`
       `git remote add origin ssh://git@bitbucket.org/#{options[:user]}/#{project_name}.git`
       `git push origin master`
    end

    say "new application setup complete"
  end
end

Setup.start(ARGV)