require 'pathname'

Dir[Pathname.new(File.dirname(__FILE__)).parent.join("app/*.rb")].each { |f| require f}